//
//  BubbleViewController.swift
//  Bubble
//
//  Created by zhangxi on 9/2/16.
//  Copyright © 2016 zhangxi.me. All rights reserved.
//

import UIKit

class BubbleViewController: UIViewController {

    
    static var mainBubble:Bubble!
    static var smallBubbles = [Bubble]()
    
    class func prepare()
    {
        mainBubble = Bubble(frame: CGRectMake(100, 100, 280, 280), name: "main_bubble", withExtension: "mov")
        

        let bubble1 = Bubble(frame: CGRectMake(100, 450, 100, 100), name: "small_bubble", withExtension: "mov")
        smallBubbles.append(bubble1)
        
        let bubble2 = Bubble(frame: CGRectMake(200, 450, 100, 100), name: "small_bubble", withExtension: "mov")
        smallBubbles.append(bubble2)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(red: 28.0/255.0, green: 29.0/255.0, blue: 68.0/255.0, alpha: 1)
        
        let bubble = BubbleViewController.mainBubble
        self.view.addSubview(bubble)
        
        
        bubble.label?.text = "Text Lable"
        bubble.subLabel?.text = "200%"
        
        bubble.play()
        //bubble.wiggle()
        
        
       for smallBubble in BubbleViewController.smallBubbles
       {
            let bubble = smallBubble
            self.view.addSubview(bubble)
            
            
            bubble.label?.text = ""
            bubble.subLabel?.text = ""
            
            bubble.play()
        
            let pan = UIPanGestureRecognizer(target: self, action: #selector(BubbleViewController.handlePan(_:)))
            bubble.addGestureRecognizer(pan)
        }

    }

    func handlePan(pan:UIPanGestureRecognizer)
    {
        let p = pan.translationInView(self.view)
        print(p)
        if(pan.state != UIGestureRecognizerState.Ended)
        {
            pan.view?.center = CGPointMake(pan.view!.center.x+p.x, pan.view!.center.y+p.y)
            pan.setTranslation(CGPointZero, inView: self.view)
        }
    }

}
