//
//  Bubble.swift
//  Bubble
//
//  Created by zhangxi on 9/2/16.
//  Copyright © 2016 zhangxi.me. All rights reserved.
//

import UIKit
import AVFoundation

class Bubble: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    var playerItem:AVPlayerItem?
    var player:AVPlayer?
    var playerLayer : AVPlayerLayer?
    
    var label:UILabel?
    var subLabel:UILabel?
    
    init(frame: CGRect,name:String?,withExtension ext:String?) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.blackColor()
        self.clipsToBounds = true
        
        if let url = NSBundle.mainBundle().URLForResource(name, withExtension: ext)
        {
            playerItem  = AVPlayerItem(URL: url)
            
            
            if playerItem != nil
            {
                player      = AVPlayer(playerItem: playerItem!)
                /*
                 case Unknown
                 case ReadyToPlay
                 case Failed
                 */
                
                playerLayer = AVPlayerLayer(player: player)
                
                playerLayer!.frame = self.bounds
                playerLayer!.videoGravity = AVLayerVideoGravityResize
                
                self.layer.addSublayer(playerLayer!)
                
            }
        }
        
        
        label = UILabel(frame: CGRectMake(0,frame.size.height/2+10,frame.size.width,20))
        subLabel = UILabel(frame: CGRectMake(0,frame.size.height/2-10,frame.size.width,20))
        
        label?.backgroundColor = UIColor.clearColor()
        subLabel?.backgroundColor = UIColor.clearColor()
        
        label?.textColor = UIColor.whiteColor()
        subLabel?.textColor = UIColor.whiteColor()
        
        label?.textAlignment = NSTextAlignment.Center
        subLabel?.textAlignment = NSTextAlignment.Center
        
        self.addSubview(label!)
        self.addSubview(subLabel!)
    }
    
    
    func play()
    {
        player?.play()
    }
    
    
    let points = [(10,10),(-10,-20),(-10,10),(0,10),(10,10),(10,-20),(-10,20)]
    var index  = 0
    
    
    func wiggle()
    {
        UIView.animateWithDuration(5, animations: { 
            
            var c = self.center
            c.x += CGFloat(self.points[self.index].0)
            c.y += CGFloat(self.points[self.index].1)
            self.center = c
            
            self.index += 1
            
            if self.index >= self.points.count
            {
                self.index = 0
            }
            
        }) { (finish) in
            if finish
            {
                self.wiggle()
            }
        }
    }

    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
}
